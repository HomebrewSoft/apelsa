from odoo import models


class AccountMove(models.Model):
    _inherit = "account.move"

    def get_invoice_line_by_name(self, product_name):
        lines = self.invoice_line_ids.filtered(
            lambda line: line.product_id.display_name == product_name
        )
        return lines[0] if lines else []
